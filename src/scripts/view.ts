class View {
    element: HTMLElement;

    createElement(tagName: string, className: string, attributes: any): HTMLElement {
        const element = document.createElement(tagName);
        element.classList.add(className);
        Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));

        return element;
    }

    createElementId(tagName: string, idName: string, attributes: any): HTMLElement {
        const element = document.createElement(tagName);
        element.id = idName;
        Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));

        return element;
    }
}

export default View;
