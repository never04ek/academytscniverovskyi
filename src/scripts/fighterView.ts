import View from './view';
import Fighter from "./Fighter";

class FighterView extends View {
    public name: string;
    public source: string;

    constructor(fighter: Fighter, handleClick: any) {
        super();

        this.createFighter(fighter, handleClick);
    }

    createFighter(fighter: Fighter, handleClick:any) {
        const name = fighter.name;
        const source = fighter.photo;
        const nameElement = this.createName(name);
        const imageElement = this.createImage(source);

        this.element = this.createElement('div', 'fighter',{});
        this.element.append(imageElement, nameElement);
        this.element.addEventListener('click', event => handleClick(event, fighter), false);
    }

    createName(name: string) {
        const nameElement = this.createElement( 'span',  'name',{});
        nameElement.innerText = name;

        return nameElement;
    }

    createImage(source: string) {
        const attributes = {src: source};
        const imgElement = this.createElement(
             'img',
             'fighter-image',
            attributes
        );

        return imgElement;
    }
}

export default FighterView;