import FightersView from './fightersView';
import {fighterService} from './services/fightersService';

export default class App {

    static rootElement = document.getElementById('root');
    static loadingElement = document.getElementById('loading-overlay');

    static startApp() {
        try {
            App.loadingElement.style.visibility = 'visible';

            const fighters = fighterService.getFighters();
            const fightersView = new FightersView(fighters);
            const fightersElement = fightersView.element;

            App.rootElement.appendChild(fightersElement);
        } catch (error) {
            console.warn(error);
            App.rootElement.innerText = 'Failed to load data';
        } finally {
            setTimeout(function () {
                App.loadingElement.style.visibility = 'hidden';
            }, 1500);
        }
    }
}

