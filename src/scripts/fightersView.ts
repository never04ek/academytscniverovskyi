import View from './view';
import FighterView from './fighterView';
import Fighter from "./Fighter";
import {fighterService} from "./services/fightersService";

export default class FightersView extends View {
    public handleClick: any;

    constructor(fighters: Array<Fighter>) {
        super();

        this.handleClick = this.handleFighterClick.bind(this);
        this.createFighters(fighters);
    }


    createFighters(fighters: Array<Fighter>) {
        const fighterElements = fighters.map(fighter => {
            const fighterView = new FighterView(fighter, this.handleClick);
            return fighterView.element;
        });

        this.element = this.createElement('div', 'fighters', {});
        this.element.append(...fighterElements);
    }

    handleFighterClick(event: Event, fighter: Fighter) {
        let modal = document.getElementById("myModal");
        modal.style.display = "block";
        window.onclick = function (event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        };

        if (!document.getElementById("health").is)
            document.getElementById("modal-content").appendChild(this.createElementId('input', 'health', {}));
        // let attack = <HTMLInputElement>document.getElementById("attack");
        // let defense = <HTMLInputElement>document.getElementById("defense");
        let health = <HTMLInputElement> document.getElementById("health");
        // attack.setAttribute("value", <string><unknown>fighterService.getFighters()[fighter._id - 1].attack);
        // defense.setAttribute("value", <string><unknown>fighterService.getFighters()[fighter._id - 1].defense);
        health.setAttribute("value", <string><unknown>fighterService.getFighters()[fighter._id - 1].health);
        document.getElementById("save-fighter-changes").addEventListener('click', () => {
            // fighter.attack = attack.value as unknown as number;
            // fighter.defense = defense.value as unknown as number;
            fighter.health = health.value as unknown as number;
            fighterService.editFighter(fighter);
            modal.style.display = "none";
        }, false);

    }
}

