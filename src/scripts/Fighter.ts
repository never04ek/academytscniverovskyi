interface Person {
    name: string;
}

export default class Fighter implements Person {
    _id: number;
    name: string;
    photo: string;
    health: number;
    attack: number;
    defense: number;

    constructor(_id: number, name: string, source: string, health: number = undefined, attack: number = undefined, defense: number = undefined) {
        this._id = _id;
        this.name = name;
        this.photo = source;
        this.attack = attack;
        this.health = health;
        this.defense = defense;
    }


}