import Fighter from "../Fighter";

class FighterService {
    public fighters: Array<Fighter> = null;

    constructor() {
        if (this.fighters == null)
            this.fighters = this.setFighters();
    }

    getFighters(): Array<Fighter> {
        return this.fighters;
    }

    editFighter(fighter: Fighter) {
        for (let i in this.fighters) {
            if (fighter._id == this.fighters[i]._id) {
                this.fighters[i] = fighter;
                return;
            }
        }
    }

    private setFighters(): Array<Fighter> {
        try {
            let request = new XMLHttpRequest();
            request.open("GET", "../src/resources/api/fighters.json", false);
            request.send(null);
            let fightersJSON = JSON.parse(request.responseText);
            let fighters: Array<Fighter> = [];
            for (let i = 0; i < fightersJSON.length; ++i) {
                fighters.push(this.getFighterDetails(fightersJSON[i]['_id']));
            }
            return fighters;
        } catch (error) {
            throw error;
        }
    }

    getFighterDetails(_id: number): Fighter {
        try {
            let request = new XMLHttpRequest();
            request.open("GET", `../src/resources/api/details/fighter/${_id}.json`, false);
            request.send(null);
            console.log(typeof JSON.parse(request.responseText));
            let fighterJSON = JSON.parse(request.responseText);
            return new Fighter(fighterJSON['_id'], fighterJSON['name'], fighterJSON['source'], fighterJSON['health'],
                fighterJSON['attack'], fighterJSON['defense']);
        } catch (error) {
            throw error;
        }
    }

}

export const fighterService = new FighterService();
